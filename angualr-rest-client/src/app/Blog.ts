export default interface Blog {
    title: string;
    body: string;
    userId: number;
    id: number;
  }