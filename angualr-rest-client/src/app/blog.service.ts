import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import Blog from './Blog';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  url = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private http: HttpClient) { }

  getBlog(): Observable<HttpResponse<Blog>> {
    return this.http.get<Blog>(
      this.url + "/1", { observe: 'response' });
  }

  postBlog(blog: Blog): Observable<Blog> {
    return this.http.post<Blog>(this.url, blog);
  }
}
