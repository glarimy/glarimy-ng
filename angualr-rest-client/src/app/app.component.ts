import { Component } from '@angular/core';
import { BlogService } from './blog.service';
import Blog from './Blog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Client with Swagger';
  blog = {
    title: "Title",
    body: "Body",
    userId: 1,
    id: 0
  };
  headers = {};

  constructor(private service: BlogService) {
    this.view();
    this.publish();
  }

  view() {
    this.service.getBlog().subscribe(resp => {
        const keys = resp.headers.keys();
        this.headers = keys.map(key =>
          `${key}: ${resp.headers.get(key)}`);
        console.log(keys);
        console.log(this.headers);
        console.log(resp.body);
        this.blog = { ...resp.body! };
      });
  }

  publish() {
    let blog: Blog = {
      title: 'glarimy',
      body: 'glarimy from ng 12',
      userId: 1,
      id: 0
    };
    this.service.postBlog(blog).subscribe(blog => console.log(blog));
  }
}
