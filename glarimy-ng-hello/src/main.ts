import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { environment } from './environments/environment';
import { AnalyzerModule } from './app/analyzer/analyzer.module';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AnalyzerModule)
  .catch(err => console.error(err));
