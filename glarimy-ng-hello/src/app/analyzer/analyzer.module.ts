import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormComponent } from '../form/form.component';
import { ListComponent } from '../list/list.component';
import { AnalyzerComponent } from '../analyzer/analyzer.component';
import { AnalyzerRoutingModule } from '../analyzer/analyzer-routing.module';

@NgModule({
  declarations: [
    FormComponent,
    ListComponent,
    AnalyzerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    AnalyzerRoutingModule
  ],
  providers: [],
  bootstrap: [AnalyzerComponent]
})
export class AnalyzerModule { }
