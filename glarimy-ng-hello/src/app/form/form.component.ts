import { Component, OnInit } from '@angular/core';
import { SyslogsService } from '../syslogs.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  syslog: string;
  constructor(private service: SyslogsService, private router: Router) { }

  ngOnInit(): void {
  }

  register() {
    this.service.add(this.syslog);
    this.syslog = undefined;
    this.router.navigate(['list']);
  }

}
