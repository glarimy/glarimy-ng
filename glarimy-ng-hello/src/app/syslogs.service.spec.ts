import { TestBed } from '@angular/core/testing';

import { SyslogsService } from './syslogs.service';

describe('SyslogsService', () => {
  let service: SyslogsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SyslogsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
