import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SyslogsService {
  entries: string[];
  constructor() {
    this.entries = [];
  }

  add(syslog: string) {
    this.entries.push(syslog);
  }

  list() {
    return this.entries;
  }
}
