import { Component, OnInit } from '@angular/core';
import { SyslogsService } from '../syslogs.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  current: string = undefined;
  constructor(private service: SyslogsService) { }

  ngOnInit(): void {
  }

  list() {
    return this.service.list();
  }

  show(entry: string) {
    this.current = entry;
  }

}
