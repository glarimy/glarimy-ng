import { Component } from '@angular/core';
import { PostsService } from '../blogs/api/posts.service';
import { Blog } from '../blogs/model/blog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string | undefined = 'Client with Swagger';

  constructor(private service: PostsService) {
    this.service.view(1, "response").subscribe(resp => { 
      console.log(resp.body);
    });
    let blog: Blog = {
      title: 'glarimy',
      body: 'glarimy from swagger 3',
      userId: 1,
      id: 0
    };
    this.service.publish(blog).subscribe(blog => {
      console.log(blog)
    });
  }
}
