#### Install Angular 12.x (Node required)

```npm install -g @angular/cli```

#### Create a new application

```ng new glarimy-app```

#### Create a new service

```ng generate service <name> --module <module>```

#### Start the application

```ng serve --open```

#### Download Swagger SDK Generator

[https://repo1.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/3.0.26/swagger-codegen-cli-3.0.26.jar](https://repo1.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/3.0.26/swagger-codegen-cli-3.0.26.jar)

#### More on Swagger Codegen

[https://github.com/swagger-api/swagger-codegen](https://github.com/swagger-api/swagger-codegen)

#### Build swagger/openapi

[https://editor.swagger.io/](https://editor.swagger.io/)

#### Generate Typescript-Angular client SDK (JDK8+ required)

```java -jar swagger-codegen-cli.jar generate -l typescript-angular -i <swagger-json-file>  -o <output-file>```

#### Scheme

![./swagger.jpg](./swagger.jpg)

#### Example

[./angular-swagger-client](./angular-swagger-client)
